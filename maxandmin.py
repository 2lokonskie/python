a = [1, 9, 7, 3, 2, 4, 5, 3]

n = len(a)
for i in range(0,n,2):
  if a[i]>a[i+1]:
    a[i], a[i+1] = a[i+1], a[i]

min = a[0]
for i in range(2,n,2):
  if a[i] < min:
    min = a[i]

max = a[1]
for i in range(3,n,2):
  if a[i] > max:
    max = a[i]

print "Minimalna = "+str(min)
print "Maksymalna = "+str(max)
